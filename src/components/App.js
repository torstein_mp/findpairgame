import React, {PureComponent} from 'react';

import "./App.styles.scss";
import cardsArray from "../utils/cardsArray";
import {shuffle} from "../utils/functions";

class App extends PureComponent {
    constructor(props) {
        super(props);
        this.curtain = React.createRef();
        this.state = {
            cards: [],
            firstCard: {},
            secondCard: {},
            cardCounter: 0
        }
    };

    componentDidMount() {
        this.curtain.current.classList.add("active");

        this.setState({cards: this.makeGameDeck(cardsArray), cardCounter: cardsArray.length}, () => {
            setTimeout(() => {
                return this.toggleShowAllCards()
            }, 1000);
            setTimeout(() => {
                this.curtain.current.classList.remove("active");
                return this.toggleShowAllCards()
            }, 4000)
        });
    };

    toggleShowAllCards = () => {

        const cards = document.querySelectorAll(".card");
        cards.forEach(card => {
            card.classList.toggle("chosen")
        })
    };

    setActiveCards = (cardCode, cardId) => ({currentTarget}) => {
        const {firstCard, secondCard} = this.state;
        const chosenCard = {
            ref: currentTarget,
            code: cardCode,
            id: cardId
        };
        if (cardId === firstCard.id || cardId === secondCard.id) {
            return console.log("this card already chosen");
        }
        currentTarget.classList.add("chosen");

        if (!firstCard.id) {
            return this.setState({firstCard: chosenCard});
        } else {
            return this.setState({secondCard: chosenCard}, () => {
                return setTimeout(() => this.checkCardsForMatch(), 1000);
            });
        }
    };

    checkCardsForMatch = () => {
        const {firstCard, secondCard} = this.state;
        return firstCard.code === secondCard.code ? this.deleteCardFromDeck(firstCard.code) : this.hideChosenCard()
    };

    hideChosenCard = () => {
        const {firstCard, secondCard} = this.state;


        return setTimeout(() => this.setState({
            firstCard: {},
            secondCard: {}
        }, () => {
            firstCard.ref.classList.remove("chosen");
            secondCard.ref.classList.remove("chosen");
        }), 2000)

    };

    deleteCardFromDeck = () => {
        const {firstCard, secondCard, cardCounter} = this.state;

        firstCard.ref.classList.add("successChosen");
        secondCard.ref.classList.add("successChosen");

        setTimeout(() => this.setState({
                firstCard: {},
                secondCard: {},
                cardCounter: (cardCounter - 1)
            }, () => {
                firstCard.ref.classList.add("hidden");
                secondCard.ref.classList.add("hidden");
            }
            ),
            2000
        )

    };
    makeGameDeck = cards => {
        const gameDeck = [...cards];

        cards.forEach(item => {
            const card = Object.assign({}, item);
            card.id += 34;
            gameDeck.push(card)
        });
        shuffle(gameDeck);
        return gameDeck
    };

    render() {
        const {cards, firstCard, secondCard, cardCounter} = this.state;
        const twoCardsChosen = firstCard.id && secondCard.id;
        return (
            <div className="App">
                <div className="curtain" ref={this.curtain}/>
                <header className="App-header">
                    <a href="/" className="App-header--resetBtn"> Reset</a>
                </header>
                <main className="App-body">
                    {cardCounter === 0 ? (
                        <div className="finalCurtain">
                            <p className="finalCurtain-title">Congratulations!</p>
                            <p className="finalCurtain-label">Level passed</p>
                            <p className="finalCurtain-label"><a href="/">Play again</a></p>
                        </div>
                    ) : (
                        <div className="desk">
                            {cards.map((card) => {
                                    return (
                                        <div className="card"
                                             key={card.id}
                                             onClick={twoCardsChosen ? undefined : this.setActiveCards(card.code, card.id)}
                                        >
                                            <img
                                                className="card-img" src={card.img}/>
                                            <div className="card-face"/>
                                        </div>
                                    )
                                }
                            )}
                        </div>
                    )
                    }
                </main>
            </div>
        );
    }
}

export default App;
