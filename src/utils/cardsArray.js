import casandra from "../dist/images/cards/card-cassandra.jpg";
import alister from  "../dist/images/cards/card-alister.jpg";
import defender from "../dist/images/cards/card-defender.jpg";
import mask from "../dist/images/cards/card-mask.jpg";


const cardsArray = [
    {
        id: 2,
        img: casandra,
        name: "Cassandra",
        code: "q1w2e3"
    },
    {
        id: 3,
        img: alister,
        name: "Alister",
        code: "q1w234"
    },
    {
        id: 4,
        img: defender,
        name: "Defender",
        code: "q12ks4"
    }
    ,
    {
        id: 5,
        img: mask,
        name: "Mask",
        code: "q3pfs4"
    }
];

export default cardsArray;